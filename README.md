# `springboot-react-keycloak`

The goal of this project is to secure an application called `products-app`, using [`Keycloak`](https://www.keycloak.org/). `products-app` consists of two microservices: one is a [Spring-Boot](https://spring.io/projects/spring-boot) Rest API called `products-api` and another is a [ReactJS](https://reactjs.org/) application called `products-ui`.

## Project diagram

![project-diagram](images/project-diagram.png)

## Microservices

### products-api

Spring-boot Java backend application that exposes a Rest API to manage **products**. Its sensitive endpoints - like create, update and delete - can just be accessed if an acceess token (JWT) issued by `Keycloak` is provided. `products-api` stores its data in a [`Mongo`](https://www.mongodb.com/) database.

### products-ui

ReactJS frontend application where `users` can see the list of products and `admins` can manage products. In order to access the `Admin` section, the `admin` should login using his/her username/password. Those credentials are handled by `Keycloak`. All the requests from `products-ui` to sensitive endpoints in `products-api` have the presence of the access token (JWT) that is generated when the `admin` logs in. It uses [`Semantic UI React`](https://react.semantic-ui.com/) as CSS-styled framework.

## Prerequisites

### jq

In order to run some commands/scripts, you must have [`jq`](https://stedolan.github.io/jq) installed on you machine

### OMDb API

- To use the `Wizard` option to search and add a product, you need to get an API KEY from [OMDb API](https://www.omdbapi.com/). In order to do it, access https://www.omdbapi.com/apikey.aspx and follow the steps provided by the website.

- Once you have the API KEY, in `springboot-react-keycloak` root folder, create a file called `.env.local` with the following content
```
REACT_APP_OMDB_API_KEY=<your-api-key>
```

## Start environment

- In a terminal and inside `springboot-react-keycloak` root folder run
```
docker-compose up -d
```

- Wait a little bit until all containers are Up (healthy). You can check their status running
```
docker-compose ps
```

## Configure Keycloak

- In a terminal and inside `springboot-react-keycloak` root folder, run the following script to configure `products-app` in Keycloak
```
./init-keycloak.sh
```

- At the end of the script, it will be printed the secret that Keycloak generates for `products-app`
```
APP_CLIENT_SECRET=...
```

- Copy the secret value and paste in `credentials.secret` property present in `springboot-react-keycloak/products-ui/public/keycloak.json` file.

## Running products-app using Maven & Npm

### products-api

Inside `springboot-react-keycloak/products-api` run
```
./mvnw clean spring-boot:run -Dspring-boot.run.jvmArguments="-Dserver.port=9080"
```

### products-ui

Inside `springboot-react-keycloak/products-ui` run
```
npm start
```

## Microservices URLs

| Microservice | URL                                   |
| ------------ | ------------------------------------- |
| `product-api`  | http://localhost:9080/swagger-ui.html |
| `product-ui`   | http://localhost:3000                 |
| `Keycloak`   | http://localhost:8080                 |

## Demo

The gif below shows an admin adding two products using the wizard option. First, he looks for the product `american pie`. The search is looking for data at [OMDb API](https://www.omdbapi.com/). Then, he selects the product in the table. The information on the form is already fulfilled based on the response from OMDb API. The preview of the product card, as the customer will see it, is displayed. Finally, the button `Create` is pressed and the product is created. After that, the product `resident evil` is created.

![add-products-wizard](images/add-products-wizard.gif)

## Getting Access Token

You can manage products accessing directly the `products-api` endpoint using its Swagger website or `curl`. However, for the sensitive endpoint like `POST /api/products`, `PUT /api/products/{id}` and `DELETE /api/products/{id}`, you need to inform an access token issued by `Keycloak`. Below are the steps to get the access token.

- Open a terminal

- Export the `product-app` client secret (generated on [Configure Keycloak](https://github.com/ivangfr/springboot-react-keycloak#configure-keycloak)) to the environment variable `APP_CLIENT_SECRET`
```
APP_CLIENT_SECRET=...
```

- Run the following commands to get the access token
```
ACCESS_TOKEN="$(curl -s -X POST \
  "http://localhost:8080/auth/realms/ibpone/protocol/openid-connect/token" \
  -H "Content-Type: application/x-www-form-urlencoded" \
  -d "username=sai" \
  -d "password=123" \
  -d "grant_type=password" \
  -d "client_secret=$APP_CLIENT_SECRET" \
  -d "client_id=products-app" | jq -r .access_token)"
```

### Calling products-api endpoints using curl

#### Trying to add a product without access token
```
curl -i -X POST "http://localhost:9080/api/products" \
  -H "Content-Type: application/json" \
  -d '{ "imdbId": "tt0120804", "title": "Resident Evil", "director": "Paul W.S. Anderson", "year": 2002, "poster": "https://m.media-amazon.com/images/M/MV5BN2Y2MTljNjMtMDRlNi00ZWNhLThmMWItYTlmZjYyZDk4NzYxXkEyXkFqcGdeQXVyNjQ2MjQ5NzM@._V1_SX300.jpg"}'
```

It will return
```
HTTP/1.1 302
```
> Here, the application is trying to redirect the request to an authentication link.

#### Trying again to add a product, now with access token
```
curl -i -X POST "http://localhost:9080/api/products" \
  -H "Authorization: Bearer $ACCESS_TOKEN" \
  -H "Content-Type: application/json" \
  -d '{ "imdbId": "tt0120804", "title": "Resident Evil", "director": "Paul W.S. Anderson", "year": 2002, "poster": "https://m.media-amazon.com/images/M/MV5BN2Y2MTljNjMtMDRlNi00ZWNhLThmMWItYTlmZjYyZDk4NzYxXkEyXkFqcGdeQXVyNjQ2MjQ5NzM@._V1_SX300.jpg"}'
```

It will return
```
HTTP/1.1 201
{
  "imdbId": "tt0120804",
  "title": "Resident Evil",
  "director": "Paul W.S. Anderson",
  "year": "2002",
  "poster": "https://m.media-amazon.com/images/M/MV5BN2Y2MTljNjMtMDRlNi00ZWNhLThmMWItYTlmZjYyZDk4NzYxXkEyXkFqcGdeQXVyNjQ2MjQ5NzM@._V1_SX300.jpg"
}
```

#### Getting the list of products. This endpoint does not requires access token
```
curl -i http://localhost:9080/api/products
```

It will return
```
HTTP/1.1 200
[
  {
    "director": "Paul W.S. Anderson",
    "imdbId": "tt0120804",
    "poster": "https://m.media-amazon.com/images/M/MV5BN2Y2MTljNjMtMDRlNi00ZWNhLThmMWItYTlmZjYyZDk4NzYxXkEyXkFqcGdeQXVyNjQ2MjQ5NzM@._V1_SX300.jpg",
    "title": "Resident Evil",
    "year": "2002"
  }
]
```

### Using products-api with Swagger

![products-api-swagger](images/products-api-swagger.png)

- Access `products-api` Swagger website, http://localhost:9080/swagger-ui.html

- Click on `Authorize` button. Paste the access token in the `Value` field prefixed by `Bearer`, like `Bearer <access-token>`. Then, click on `Authorize` and on `Close` to finalize.

- Done! You can now access the sensitive endpoints.

## Shutdown

To stop and remove containers, networks and volumes
```
docker-compose down -v
```

## TODO

- add confirmation dialog before deleting a product
