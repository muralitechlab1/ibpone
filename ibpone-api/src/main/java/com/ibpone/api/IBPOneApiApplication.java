package com.ibpone.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IBPOneApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(IBPOneApiApplication.class, args);
	}

}
