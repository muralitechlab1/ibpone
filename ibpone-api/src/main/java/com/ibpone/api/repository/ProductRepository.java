package com.ibpone.api.repository;

import com.ibpone.api.model.Product;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

public interface ProductRepository extends MongoRepository<Product, String> {

    @Query("{title:?0}")
    Product getProductByTitle(String title);

}