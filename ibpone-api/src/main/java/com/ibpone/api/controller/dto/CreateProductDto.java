package com.ibpone.api.controller.dto;

import javax.validation.constraints.NotBlank;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class CreateProductDto {

  @ApiModelProperty(example = "tt0120804")
  @NotBlank
  private String imdbId;

  @ApiModelProperty(position = 2)
  @NotBlank
  private String title;

  @ApiModelProperty(position = 3)
  @NotBlank
  private String category;

  @ApiModelProperty(position = 4)
  @NotBlank
  private String year;

  @ApiModelProperty(position = 5)
  private String location;

}