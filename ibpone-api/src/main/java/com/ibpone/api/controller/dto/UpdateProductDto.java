package com.ibpone.api.controller.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class UpdateProductDto {

  @ApiModelProperty()
  private String title;

  @ApiModelProperty(position = 2)
  private String category;

  @ApiModelProperty(position = 3)
  private String year;

  @ApiModelProperty(position = 4)
  private String location;

}