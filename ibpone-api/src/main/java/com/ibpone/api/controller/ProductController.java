package com.ibpone.api.controller;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import com.ibpone.api.exception.ProductNotFoundException;
import com.ibpone.api.model.Product;
import com.ibpone.api.controller.dto.CreateProductDto;
import com.ibpone.api.controller.dto.ProductDto;
import com.ibpone.api.controller.dto.UpdateProductDto;
import com.ibpone.api.service.ProductService;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import ma.glasnost.orika.MapperFacade;

@RestController
@RequestMapping("/api/products")
public class ProductController {

  private final ProductService productService;
  private final MapperFacade mapperFacade;




//    @RequestMapping(value = {"/","/home"})
//    public String index() {
//      return "index";
//    }


  public ProductController(ProductService productService, MapperFacade mapperFacade) {
    this.productService = productService;
    this.mapperFacade = mapperFacade;
  }

  @GetMapping
  public List<ProductDto> getProducts() {
    return productService.getProducts().stream().map(product -> mapperFacade.map(product, ProductDto.class))
        .collect(Collectors.toList());
  }

  @GetMapping("/{imdbId}")
  public ProductDto getProduct(@PathVariable String imdbId) throws ProductNotFoundException {
    Product product = productService.validateAndGetProduct(imdbId);
    return mapperFacade.map(product, ProductDto.class);
  }

  @ResponseStatus(HttpStatus.CREATED)
  @PostMapping
  public ProductDto createProduct(@Valid @RequestBody CreateProductDto createProductDto) {
    Product product = mapperFacade.map(createProductDto, Product.class);
    product = productService.saveProduct(product);
    return mapperFacade.map(product, ProductDto.class);
  }

  @PutMapping("/{imdbId}")
  public ProductDto updateProduct(@PathVariable String imdbId, @Valid @RequestBody UpdateProductDto updateProductDto)
      throws ProductNotFoundException {
    Product product = productService.validateAndGetProduct(imdbId);
    mapperFacade.map(updateProductDto, product);
    product = productService.saveProduct(product);
    return mapperFacade.map(product, ProductDto.class);
  }

  @DeleteMapping("/{imdbId}")
  public ProductDto deleteProduct(@PathVariable String imdbId) throws ProductNotFoundException {
    Product product = productService.validateAndGetProduct(imdbId);
    productService.deleteProduct(product);
    return mapperFacade.map(product, ProductDto.class);
  }

  @GetMapping("/{title}")
  public ProductDto getProductByTitle(@PathVariable String title) throws ProductNotFoundException {
    Product product = productService.getProductByTitle(title);
    return mapperFacade.map(product, ProductDto.class);
  }

}