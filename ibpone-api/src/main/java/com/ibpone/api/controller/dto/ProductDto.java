package com.ibpone.api.controller.dto;

import lombok.Data;

@Data
public class ProductDto {

  private String imdbId;
  private String title;
  private String category;
  private String year;
  private String location;

}