package com.ibpone.api.config;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.config.annotation.*;

@Controller
public class webConfig implements WebMvcConfigurer {

    @RequestMapping(value = "/{path:^(?!api$)(?!static$)(?!webjars$)[^\\.]*}/**")
    public String redirect() {
        return "forward:/index.html";
    }


}
