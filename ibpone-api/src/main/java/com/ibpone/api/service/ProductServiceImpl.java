package com.ibpone.api.service;

import java.util.List;

import com.ibpone.api.exception.ProductNotFoundException;
import com.ibpone.api.model.Product;
import com.ibpone.api.repository.ProductRepository;

import org.springframework.stereotype.Service;

@Service
public class ProductServiceImpl implements ProductService {

  private final ProductRepository productRepository;

  public ProductServiceImpl(ProductRepository productRepository) {
    this.productRepository = productRepository;
  }

  @Override
  public Product validateAndGetProduct(String imdbId) throws ProductNotFoundException {
    return productRepository.findById(imdbId).orElseThrow(() -> new ProductNotFoundException(String.format("Product with imdbId '%s' not found", imdbId)));
  }

  @Override
  public List<Product> getProducts()
  {
    return productRepository.findAll();
  }

  @Override
  public Product saveProduct(Product product) {

    return productRepository.save(product);
  }

  @Override
  public void deleteProduct(Product product) {

    productRepository.delete(product);
  }

  @Override
  public Product getProductByTitle(String title) throws ProductNotFoundException {

    return productRepository.getProductByTitle(title);
  }

}