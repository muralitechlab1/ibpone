package com.ibpone.api.service;

import java.util.List;

import com.ibpone.api.exception.ProductNotFoundException;
import com.ibpone.api.model.Product;

public interface ProductService {

  Product validateAndGetProduct(String imdbId) throws ProductNotFoundException;

  List<Product> getProducts();

  Product saveProduct(Product product);

  void deleteProduct(Product product);

  Product getProductByTitle(String title) throws ProductNotFoundException;
}