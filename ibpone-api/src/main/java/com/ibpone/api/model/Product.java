package com.ibpone.api.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Data
@Document(collection = "products")
public class Product {

  @Id
  private String imdbId;
  private String title;
  private String category;
  private String year;
  private String location;

}