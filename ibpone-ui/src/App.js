import React from 'react'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import { KeycloakProvider } from 'react-keycloak'
import Keycloak from 'keycloak-js'
import Navbar from './components/misc/Navbar'
import Home from './components/home/Home'
import Product from './components/product/Product'
import ProductWizard from './components/wizard/ProductWizard'

const keycloak = Keycloak('/keycloak.json', { onLoad: 'login-required', promiseType: 'native' })

function App() {
  return (
    <Router>
      <KeycloakProvider keycloak={keycloak}>
        <Navbar />
        <Route path='/' exact component={Home} />
        <Route path='/home' exact component={Home} />
        <Route path='/products' exact component={Product} />
        <Route path='/wizard' exact component={ProductWizard} />
      </KeycloakProvider>
    </Router>
  )
}

export default App