import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'
import { Menu, Container, Dropdown } from 'semantic-ui-react'
import { withRouter } from 'react-router-dom'
import { withKeycloak } from 'react-keycloak'

class Navbar extends Component {

  handleLogInOut = () => {
    const { keycloak, history } = this.props
    if (keycloak.authenticated) {
      history.push('/')
      keycloak.logout()
    } else {
      keycloak.login()
    } 
  }

  checkAuthenticated = () => {
    const { keycloak } = this.props
    if (!keycloak.authenticated) {
      this.handleLogInOut()
    }
  }

  render() {
    const { keycloak } = this.props
    const logInOut = keycloak.authenticated ? "Logout" : "Login"
    const adminMenuVisibility = keycloak.authenticated ? { "display": "block" } : { "display": "none" }
    return (
      <Menu>
        <Container>
          <Menu.Item header>Products UI</Menu.Item>
          <Menu.Item>Product</Menu.Item>
          <Menu.Item as={NavLink} exact to="/home">Home</Menu.Item>
          <Dropdown item text='Admin' style={adminMenuVisibility}>
            <Dropdown.Menu>
              <Dropdown.Item as={NavLink} exact to="/products" onClick={this.checkAuthenticated}>Products</Dropdown.Item>
              <Dropdown.Item as={NavLink} exact to="/wizard" onClick={this.checkAuthenticated}>Product Wizard</Dropdown.Item>
            </Dropdown.Menu>
          </Dropdown>
          <Menu.Item as={NavLink} exact to="/login" onClick={this.handleLogInOut}>{logInOut}</Menu.Item>
        </Container>
      </Menu >
    )
  }
}

export default withRouter(withKeycloak(Navbar))