import React from 'react'
import { Form, Button } from 'semantic-ui-react'

function ProductForm({ form, handleChange, saveProduct, clearForm }) {
  return (
    <Form>
      <Form.Input
        fluid
        label='ImdbID'
        id='imdbId'
        onChange={handleChange}
        value={form.imdbId}
        error={form.imdbIdError}
      />
      <Form.Input
        fluid
        label='Title'
        id='title'
        onChange={handleChange}
        value={form.title}
        error={form.titleError}
      />

      <Form.Input
        fluid
        label='Category'
        id='category'
        onChange={handleChange}
        value={form.category}
        error={form.categoryError}
      />
      <Form.Input
        fluid
        label='Year'
        id='year'
        onChange={handleChange}
        value={form.year}
        error={form.yearError}
      />
      <Form.Input
        fluid
        label='Location'
        id='location'
        onChange={handleChange}
        value={form.location}
        error={form.locationError}
      />
      <Button.Group fluid>
        <Button onClick={clearForm}>Cancel</Button>
        <Button.Or />
        <Button positive onClick={saveProduct}>Save</Button>
      </Button.Group>
    </Form>
  )
}

export default ProductForm