import React from 'react'
import { Table, Button, Image } from 'semantic-ui-react'

function ProductTable({ products, deleteProduct, editProduct }) {
  const productList = products && products.map(product => {
    return (
      <Table.Row key={product.imdbId}>
        <Table.Cell collapsing>
          <Button
            circular
            color='red'
            size='small'
            icon='trash'
            onClick={() => deleteProduct(product.imdbId)}
          />
          <Button
            circular
            color='orange'
            size='small'
            icon='edit'
            onClick={() => editProduct(product)}
          />
        </Table.Cell>
        <Table.Cell>{product.imdbId}</Table.Cell>
        <Table.Cell>{product.title}</Table.Cell>
        <Table.Cell>{product.category}</Table.Cell>
        <Table.Cell>{product.year}</Table.Cell>
        <Table.Cell>
          <Image src={product.location} rounded size='tiny' />
        </Table.Cell>
      </Table.Row>
    )
  })

  return (
    <Table compact striped>
      <Table.Header>
        <Table.Row>
          <Table.HeaderCell />
          <Table.HeaderCell>ImdbID</Table.HeaderCell>
          <Table.HeaderCell>Title</Table.HeaderCell>
          <Table.HeaderCell>category</Table.HeaderCell>
          <Table.HeaderCell>Year</Table.HeaderCell>
          <Table.HeaderCell>location</Table.HeaderCell>
        </Table.Row>
      </Table.Header>
      <Table.Body>
        {productList}
      </Table.Body>
    </Table>
  )
}

export default ProductTable