import React, { Component } from 'react'
import { Container, Grid, Segment, Header } from 'semantic-ui-react'
import { withKeycloak } from 'react-keycloak'
import ProductForm from './ProductForm'
import ProductTable from './ProductTable'
import productsApi from '../misc/products-api'

class Product extends Component {
  formInitialState = {
    imdbId: '',
    title: '',
    category: '',
    year: '',
    location: '',

    imdbIdError: false,
    titleError: false,
    categoryError: false,
    yearError: false,
    locationError: false,
  }

  state = {
    products: [],
    form: { ...this.formInitialState },
  }

  componentDidMount() {
    this.getAllProducts()
  }

  handleChange = (e) => {
    const { id, value } = e.target
    const form = { ...this.state.form }
    form[id] = value
    this.setState({ form })
  }

  getAllProducts = () => {
    productsApi.get('products')
      .then(response => {
        const products = response.data
        this.setState({ products })
      })
  }

  saveProduct = () => {
    if (!this.isValidForm()) {
      return
    }

    const { keycloak } = this.props
    const { imdbId, title, category, year, location } = this.state.form
    const product = { imdbId: imdbId, title: title, category: category, year: year, location: location }

    productsApi.post('products', product, {
      headers: {
        'Content-type': 'application/json',
        'Authorization': 'Bearer ' + keycloak.token
      }
    })
      .then(() => {
        this.clearForm()
        console.log(keycloak.token)
        this.getAllProducts()
      })
      .catch(error => {
        console.log(error)
      })
  }

  deleteProduct = (id) => {
    const { keycloak } = this.props
    productsApi.delete(`products/${id}`, {
      headers: {
        'Content-type': 'application/json',
        'Authorization': 'Bearer ' + keycloak.token
      }
    })
      .then(() => {
        this.getAllProducts()
      })
      .catch(error => {
        console.log(error)
      })
  }

  editProduct = (product) => {
    const form = {
      imdbId: product.imdbId,
      title: product.title,
      category: product.category,
      year: product.year,
      location: product.location,
      imdbIdError: false,
      titleError: false,
      categoryError: false,
      yearError: false,
      locationError: false,
    }
    this.setState({ form })
  }

  clearForm = () => {
    this.setState({
      form: { ...this.formInitialState }
    })
  }

  isValidForm = () => {
    const form = { ...this.state.form }
    const imdbIdError = form.imdbId.trim() === ''
    const titleError = form.title.trim() === ''
    const categoryError = form.category.trim() === ''
    const yearError = form.year.trim() === ''
    const locationError = form.location.trim() === ''

    form.imdbIdError = imdbIdError
    form.titleError = titleError
    form.categoryError = categoryError
    form.yearError = yearError
    form.locationError = locationError

    this.setState({ form })
    return (imdbIdError || titleError || categoryError || yearError || locationError) ? false : true
  }

  render() {
    return (
      <Container>
        <Header as='h3' textAlign='center'>Products</Header>
        <Grid>
          <Grid.Column mobile={16} tablet={16} computer={4}>
            <Segment>
              <ProductForm
                form={this.state.form}
                handleChange={this.handleChange}
                saveProduct={this.saveProduct}
                clearForm={this.clearForm}
              />
            </Segment>
          </Grid.Column>
          <Grid.Column mobile={16} tablet={16} computer={12}>
            <Segment>
              <ProductTable
                products={this.state.products}
                deleteProduct={this.deleteProduct}
                editProduct={this.editProduct}
              />
            </Segment>
          </Grid.Column>
        </Grid>
      </Container>
    )
  }
}

export default withKeycloak(Product)