import React from 'react'
import { Segment, Form } from 'semantic-ui-react'

function FormStep({ imdbId, title, category, year, location, imdbIdError, titleError, categoryError, yearError, locationError, handleChange }) {
  return (
    <Segment>
      <Form>
        <Form.Input
          label='IMDB ID'
          id='imdbId'
          onChange={handleChange}
          value={imdbId}
          error={imdbIdError}
        />
        <Form.Input
          fluid
          label='Title'
          id='title'
          onChange={handleChange}
          value={title}
          error={titleError}
        />
        <Form.Input
          fluid
          label='category'
          id='category'
          onChange={handleChange}
          value={category}
          error={categoryError}
        />
        <Form.Input
          label='Year'
          id='year'
          onChange={handleChange}
          value={year}
          error={yearError}
        />
        <Form.Input
          fluid
          label='location'
          id='location'
          onChange={handleChange}
          value={location}
          error={locationError}
        />
      </Form>
    </Segment>
  )
}

export default FormStep
