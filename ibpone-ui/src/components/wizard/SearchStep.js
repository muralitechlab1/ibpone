import React from 'react'
import { Segment, Table, Form } from 'semantic-ui-react'

function SearchStep({ search, isLoading, products, selectedProduct, handleChange, searchProducts, handleTableSelection }) {
  const productList = products ? products.map(product => {
    const active = product && selectedProduct && product.imdbId === selectedProduct.imdbId ? true : false
    return (
      <Table.Row
        key={product.imdbId}
        active={active}
        onClick={() => handleTableSelection(product)}
      >
        <Table.Cell>{product.imdbId}</Table.Cell>
        <Table.Cell>{product.title}</Table.Cell>
        <Table.Cell>{product.category}</Table.Cell>
        <Table.Cell>{product.year}</Table.Cell>
      </Table.Row>
    )
  }) : (
      <Table.Row>
        <Table.Cell></Table.Cell>
      </Table.Row>
    )

  return (
    <Segment loading={isLoading}>
      <Form onSubmit={searchProducts}>
        <Form.Group widths='equal'>
          <Form.Input
            fluid
            placeholder='Search for a product title ...'
            id='search'
            value={search}
            onChange={handleChange}
          />
          <Form.Button primary content='Search' />
        </Form.Group>
      </Form>

      <Table compact selectable>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>ImdbID</Table.HeaderCell>
            <Table.HeaderCell>Title</Table.HeaderCell>
            <Table.HeaderCell>category</Table.HeaderCell>
            <Table.HeaderCell>Year</Table.HeaderCell>
          </Table.Row>
        </Table.Header>

        <Table.Body>
          {productList}
        </Table.Body>
      </Table>
    </Segment>
  )
}

export default SearchStep