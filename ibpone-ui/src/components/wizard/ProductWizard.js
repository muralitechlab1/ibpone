import React, { Component } from 'react'
import { Container, Grid, Responsive, Segment, Step, Button, Icon } from 'semantic-ui-react'
import { withKeycloak } from 'react-keycloak'
import SearchStep from './SearchStep'
import FormStep from './FormStep'
import CompleteStep from './CompleteStep'
import omdbApi from '../misc/omdb-api'
import productsApi from '../misc/products-api'

class ProductWizard extends Component {
  state = {
    step: 1,

    // Search Step
    isLoading: false,
    search: '',
    products: [],
    selectedProduct: null,

    // Form Step
    imdbId: '',
    title: '',
    category: '',
    year: '',
    location: '',
    imdbIdError: false,
    titleError: false,
    categoryError: false,
    yearError: false,
    locationError: false
  }

  previousStep = () => {
    let { step } = this.state

    let { imdbIdError, titleError, categoryError, yearError, locationError } = this.state
    if (step === 2) {
      imdbIdError = false
      titleError = false
      categoryError = false
      yearError = false
      locationError = false
    }

    step = step > 1 ? step - 1 : step
    this.setState({ step, imdbIdError, titleError, categoryError, yearError, locationError })
  }

  nextStep = () => {
    let { step } = this.state

    if (step === 2 && !this.isValidForm()) {
      return
    }

    step = step < 3 ? step + 1 : step
    this.setState({ step })
  }

  handleChange = (e) => {
    const { id, value } = e.target
    this.setState({ [id]: value })
  }

  handleTableSelection = (product) => {
    const { selectedProduct } = this.state

    if (product && selectedProduct && product.imdbId === selectedProduct.imdbId) {
      this.setState({
        selectedProduct: null,
        imdbId: '',
        title: '',
        category: '',
        year: '',
        location: ''
      })
    } else {
      this.setState({
        selectedProduct: product,
        imdbId: product.imdbId,
        title: product.title,
        category: product.category,
        year: product.year,
        location: product.location
      })
    }
  }

  searchProducts = () => {
    this.setState({ isLoading: true })

    omdbApi.get(`?apikey=${process.env.REACT_APP_OMDB_API_KEY}&t=${encodeURI(this.state.search)}`)
      .then(response => {
        let products = []
        const { Error } = response.data
        if (Error) {
          console.log(Error)
        } else {
          const product = {
            imdbId: response.data.imdbID,
            title: response.data.Title,
            category: response.data.category,
            year: response.data.Year,
            location: response.data.location
          }
          products.push(product)
        }
        this.setState({
          products,
          isLoading: false
        })
      })
      .catch(error => {
        console.log(error)
      })
  }

  createProduct = (product) => {
    const { keycloak } = this.props

    productsApi.post('products', product, {
      headers: {
        'Content-type': 'application/json',
        'Authorization': 'Bearer ' + keycloak.token
      }
    })
      .then((response) => {
        this.props.history.push("/home")
      })
      .catch(error => {
        console.log(error)
      })
  }

  isValidForm = () => {
    const imdbIdError = this.state.imdbId.trim() === ''
    const titleError = this.state.title.trim() === ''
    const categoryError = this.state.category.trim() === ''
    const yearError = this.state.year.trim() === ''
    const locationError = this.state.location.trim() === ''

    this.setState({ imdbIdError, titleError, categoryError, yearError, locationError })
    return imdbIdError || titleError || categoryError || yearError || locationError ? false : true
  }

  getContent = () => {
    const { step } = this.state

    let stepContent = null
    if (step === 1) {
      const { isLoading, search, products, selectedProduct } = this.state
      stepContent = (
        <SearchStep
          search={search}
          isLoading={isLoading}
          products={products}
          selectedProduct={selectedProduct}
          handleChange={this.handleChange}
          searchProducts={this.searchProducts}
          handleTableSelection={this.handleTableSelection}
        />
      )
    } else if (step === 2) {
      const { imdbId, title, category, year, location, imdbIdError, titleError, categoryError, yearError, locationError } = this.state
      stepContent = (
        <FormStep
          imdbId={imdbId}
          title={title}
          category={category}
          year={year}
          location={location}
          imdbIdError={imdbIdError}
          titleError={titleError}
          categoryError={categoryError}
          yearError={yearError}
          locationError={locationError}
          handleChange={this.handleChange}
        />
      )
    } else if (step === 3) {
      const { imdbId, title, category, year, location } = this.state
      const product = { imdbId, title, category, year, location }
      stepContent = (
        <CompleteStep
          product={product}
          createProduct={this.createProduct}
        />
      )
    }

    return (
      <Container>
        <Grid>
          <Grid.Column mobile={16} tablet={4} computer={4}>
            <Responsive as={Segment} minWidth="768">
              <Step.Group fluid vertical >
                <Step active={step === 1}>
                  <Icon name='search' />
                  <Step.Content>
                    <Step.Title>Search</Step.Title>
                    <Step.Description>Search product</Step.Description>
                  </Step.Content>
                </Step>

                <Step active={step === 2}>
                  <Icon name='film' />
                  <Step.Content>
                    <Step.Title>Product</Step.Title>
                    <Step.Description>Product Form</Step.Description>
                  </Step.Content>
                </Step>

                <Step active={step === 3}>
                  <Icon name='flag checkered' />
                  <Step.Content>
                    <Step.Title>Complete</Step.Title>
                    <Step.Description>Preview and complete</Step.Description>
                  </Step.Content>
                </Step>
              </Step.Group>
            </Responsive>

            <Button.Group fluid>
              <Button
                disabled={step === 1}
                onClick={this.previousStep}>Back</Button>
              <Button.Or />
              <Button
                positive
                disabled={step === 3}
                onClick={this.nextStep}>Next</Button>
            </Button.Group>
          </Grid.Column>
          <Grid.Column mobile={16} tablet={12} computer={12}>
            {stepContent}
          </Grid.Column>
        </Grid>
      </Container>
    )
  }

  render() {
    const { keycloak } = this.props
    const { authenticated } = keycloak
    return keycloak && authenticated ? this.getContent() : <div></div>
  }
}

export default withKeycloak(ProductWizard)