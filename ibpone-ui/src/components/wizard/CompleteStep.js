import React from 'react'
import { Segment, Form } from 'semantic-ui-react'
import ProductCard from '../home/ProductCard'

function CompleteStep({ product, createProduct }) {
  return (
    <Segment compact>
      <ProductCard product={product} />
      <Form>
        <Form.Button fluid primary onClick={() => createProduct(product)}>Create</Form.Button>
      </Form>
    </Segment>
  )
}

export default CompleteStep