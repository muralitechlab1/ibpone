import React from 'react'
import { Card, Image } from 'semantic-ui-react'

function ProductCard({ product }) {
  return (
    <Card>
      <Image src={product.location} wrapped ui={false} />
      <Card.Content textAlign="center">
        <Card.Header>{product.title}</Card.Header>
      </Card.Content>
      <Card.Content>
        <Card.Description>imdbID: <strong>{product.imdbId}</strong></Card.Description>
        <Card.Description>Author: <strong>{product.category}</strong></Card.Description>
        <Card.Description>Year: <strong>{product.year}</strong></Card.Description>
      </Card.Content>
    </Card>
  )
}

export default ProductCard