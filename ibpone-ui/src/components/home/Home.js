import React, { Component } from 'react'
import { Container } from 'semantic-ui-react'
import productsApi from '../misc/products-api'
import ProductList from './ProductList'

class Home extends Component {
  state = {
    isLoading: false,
    products: []
  }

  componentDidMount() {
    this.getProducts()
  }

  getProducts = () => {
    this.setState({ isLoading: true })

    productsApi.get('products')
      .then(response => {
        const products = response.data
        this.setState({
          isLoading: false,
          products
        })
      })
      .catch(error => {
        console.log(error)
      })
  }

  render() {
    const { isLoading, products } = this.state
    return (
      <Container>
        <ProductList isLoading={isLoading} products={products} />
      </Container>
    )
  }
}

export default Home