import React from 'react'
import { Card, Header, Segment } from 'semantic-ui-react'
import ProductCard from './ProductCard'

function ProductList({ isLoading, products }) {
  if (isLoading) {
    return null
  }

  const productList = products && products.map(product => {
    return (
      <ProductCard
        key={product.imdbId}
        product={product}
      />
    )
  })

  return (
    products && products.length > 0 ? (
      <Card.Group doubling centered>
        {productList}
      </Card.Group >
    ) : (
        <Segment padded color='blue'>
          <Header textAlign='center' as='h4'>No products</Header>
        </Segment>
      )
  )
}

export default ProductList